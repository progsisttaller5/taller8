ejecutables:		bin/estatico bin/dinamico

bin/estatico:		obj/main.o lib/libcriptops.a
			gcc -Wall $^ -o $@
bin/dinamico:		obj/main.o lib/libcriptops.so
			gcc -Iinclude/ -Wall obj/main.o -o $@ -L./lib -lcriptops
obj/main.o: 		src/main.c
			gcc -Iinclude/ -Wall $< -c -o $@
lib/libcriptops.so:	obj/encriptacion_din.o obj/morse_din.o
			LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/xidrovo/Documents/taller8/lib
			export LD_LIBRARY_PATH
			gcc -shared -o $@ obj/encriptacion_din.o obj/morse_din.o

lib/libcriptops.a:	obj/encriptacion.o obj/morse.o
			ar rcs $@ $^

obj/encriptacion_din.o:	src/encriptacion.c
			gcc -Wall -fPIC $< -c -o $@
obj/morse_din.o:	src/morse.c
			gcc -Wall -fPIC $< -c -o $@

obj/encriptacion.o:	src/encriptacion.c
			gcc -Wall $< -c -o $@
obj/morse.o:		src/morse.c
			gcc -Wall $< -c -o $@


clean:			
			rm obj/*.o bin/* lib/*
run-static:		bin/estatico
			./bin/estatico
run-dinamic:		bin/dinamico
			./bin/dinamico
