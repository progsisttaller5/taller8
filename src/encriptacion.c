#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

char* cifrado_ciclico(char* nombre, int avance){
	
	int fixedAdvance = avance;
	//Esto evitara el sobrepase del vocabulario.
	while (fixedAdvance > 25){
		fixedAdvance = fixedAdvance - 25;
	}
	for (int i=0; i<strlen(nombre);i++){
		if (nombre[i]=='\0'){
			break;
		}
		 	
		if(((int)nombre[i]>=65 && (int)nombre[i]<90) || ((int)nombre[i]>=97 && (int)nombre[i]<122)){ 
		
			nombre[i]= (int)nombre[i] + fixedAdvance;
		
		}
		if( ( (int)nombre[i] >= 90 && (int)nombre[i] < 97 ) || (int)nombre[i] > 122){
			nombre[i] = (int)nombre[i] - 25;
		}	
	}
	return nombre;
}
